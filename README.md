# Levantamiento de las Apps #

##7minWorkout##
1. Instalar http-server en caso de no tenerlo instalado
1. En el directorio "app" correr http-server para levantar servidor local
1. Ver en el navegador en http://localhost:[puerto que te de el http-server]/app/#/workout

##chat##
1. Instalar http-server en caso de no tenerlo instalado
1. En el directorio "app" correr http-server para levantar servidor local
1. Ver en el navegador en http://localhost:[puerto que te de el http-server]/

##store-sports##
1. Instalar deployd
1. En el directorio "sportsstore" correr dpd -p 5500 app.dpd
1. En el directorio "angular" correr node server.js
1. Ver el navegador en localhost:5000