// Custom Service
angular.module('cart', [])
.factory('cart', function(){
  var cartData = [];
  return {
    addProduct: function (id, name, price) {
      var addedToExistingItem = false;
      for (var i = 0; i < cartData.length; i++) {
        if (cartData[i].id == id) {
          cartData[i].count++;
          addedToExistingItem = true;
          // console.log('cartData[i]:');
          // console.log(cartData[i]);
          // console.log('------------------------');
          break;
        }
      }

      if (!addedToExistingItem) {
        // console.log("No se ha agregado al elemento existente");
        cartData.push({
          count: 1, id: id, price: price, name: name
        });
        // console.log("cartData: ");
        // console.log(cartData);
        // console.log('-------------------------');
      }
    },

    removeProduct: function (id) {
      for (var i = 0; i < cartData.length; i++) {
        if (cartData[i].id == id) {
          cartData.splice(i, 1);
          break;
        }
      }
    },

    getProducts: function () {
      // console.log('return cartData:');
      // console.log(cartData);
      return cartData;
    }
  }
})
// Widget
.directive('cartSummary', function (cart) {
  return {
    restrict: 'E',
    templateUrl: 'components/cart/cartSummary.html',
    controller: function($scope){
      var cartData = cart.getProducts();

      $scope.total = function(){
        var total = 0;
        for (var i = 0; i < cartData.length; i++) {
          total += (cartData[i].price * cartData[i].count);
        }
        return total;
      }

      $scope.itemCount = function(){
        var total = 0;
        for (var i = 0; i < cartData.length; i++) {
          total += cartData[i].count;
        }
        return total;
      }
    }
  }
})
;
