angular.module("sportsStoreAdmin")
.constant("authUrl", "http://localhost:5500/users/")
.constant("ordersUrl", "http://localhost:5500/orders/")
.controller("authCtrl", function ($scope, $http, $location, authUrl) {
  $scope.authenticate = function (user, pass) {
    $http.get(authUrl, {
      username: user,
      password: pass
    },{
      withCredentials: true
    })
    .success(function(data){
      // console.log("$location.path");
      // console.log($location.path);
      var noAuth = {
        message: "Bad username or password. You don't have permissions",
        status: 401,
        statusCode: 401
      };
      /**/
      if(data[0].username === user && data[0].password === pass){
        $location.path("/main");
      }else{
        $scope.authenticationError = noAuth;
      }
      /**/
      // console.log("data:");
      // console.log(data[0].username);
      // console.log(data[0].password);
      //
      // console.log("user:");
      // console.log(user);
      //
      // console.log("pass:");
      // console.log(pass);
    })
    .error(function (error) {
      //  console.log("error");
      //  console.log(error);
      //  $scope.error = error;
      $scope.authenticationError = error;
    });
  }
})
.controller("mainCtrl", function($scope){
  $scope.screens = ["Products", "Orders"];
  $scope.current = $scope.screens[0];

  $scope.setScreen = function (index) {
    $scope.current=  $scope.screens[index];
  };

  $scope.getScreen = function(){
    return $scope.current == "Products" ? "/views/adminProducts.html" : "/views/adminOrders.html";
  };

})
.controller("ordersCtrl", function($scope, $http, ordersUrl){
  $http.get(ordersUrl, {withCredentials: true})
    .success(function(data){
      $scope.orders = data;
    })
    .error(function(error){
      $scope.error = error;
    });
  $scope.selectedOrder;
  $scope.fullTotal;
  $scope.selectOrder = function(order){
    $scope.selectedOrder = order;
  };
  $scope.calcTotal = function(order){
    var total = 0;
    for (var i = 0; i < order.products.length; i++) {
      total += order.products[i].count * order.products[i].price;
      $scope.fullTotal = total;
    }
    return total;
  };
})
;
