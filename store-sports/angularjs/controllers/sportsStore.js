angular.module("sportsStore")
.constant("dataUrl", "http://localhost:5500/products")
.constant("orderUrl", "http://localhost:5500/orders")
.controller("sportsStoreCtrl", function($scope, $http, $location, dataUrl, orderUrl, cart){
  $scope.data = {};
  $scope.productsList = "../views/productsList.html";

  $http.get(dataUrl)
  .success(function(data){
    $scope.data.products = data;
  })
  .error(function(error){
    $scope.data.error = error;
    console.log('Error:');
    console.log(error);
  });

  $scope.sendOrder = function(shippingDetails){
    var order = angular.copy(shippingDetails);

    // console.log("shippingDetails");
    // console.log(shippingDetails);
    //
    // console.log("order");
    // console.log(order);

    order.products = cart.getProducts();

    // console.log("order.products");
    // console.log(order.products);


    $http.post(orderUrl, order)
      .success(function(data){
        console.log("$scope.data");
        console.log($scope.data);
        $scope.data.orderId = data.id;
        cart.getProducts().length = 0;
      })
      .error(function(error){
        $scope.data.orderError = error;
      })
      .finally(function(){
        $location.path("/complete")
      });

  };
  ;
})
;
