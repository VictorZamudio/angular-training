angular.module("sportsStoreAdmin")
.constant("productUrl", "http://localhost:5500/products")
.config(function($httpProvider){
  $httpProvider.defaults.withCredentials = true;
})
.controller("productCtrl", function ($scope, $resource, $http, productUrl) {
  $scope.productsResource = $resource(productUrl + "/:id", {id: "@id"});
  // console.log( productUrl + ":id", {id: "@id"} );

  $scope.listProducts = function () {
    $scope.products = $scope.productsResource.query();
    //  console.log($scope.products);
  };

  $scope.deleteProduct = function (product) {
    product.$delete().then(function(){
      $scope.products.splice($scope.products.indexOf(product), 1);
    });
    // $http.delete(productUrl + "/" + product.id)
    // .success(function () {
    //   $scope.products.splice($scope.products.indexOf(product), 1);
    // })
    // .error(function () {
    //
    // });
  };

  $scope.createProduct = function (product) {
    new $scope.productsResource(product).$save().then(function(newProduct){
      $scope.products.push(newProduct);
      $scope.editedProduct = null;
    });
  };

  $scope.updateProduct = function (product) {
    product.$save();
    $scope.editedProduct = null;
    // $http.put(productUrl + "/" + product.id).success(function () {
    //   console.log(product.price);
    //   $scope.products.splice($scope.products.indexOf(product), 1);
    // });
  };

  $scope.startEdit = function(product) {
    $scope.editedProduct = product;
  };

  $scope.cancelEdit = function() {
    $scope.editedProduct = null;
  };

  $scope.listProducts();

})
;
