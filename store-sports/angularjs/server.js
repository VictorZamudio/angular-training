var connect = require('connect');
var serveStatic = require('serve-static');
var app = connect();
var dir = '../angularjs';

app.use( serveStatic(dir) )
.listen(5000);
// console.log('Server listen on port 5000');
