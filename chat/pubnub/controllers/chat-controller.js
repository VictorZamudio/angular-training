angular.module('app', ["pubnub.angular.service"])
.controller('ChatCtrl', function ($scope, Pubnub) {
  $scope.channel = 'messages-channel';
  // Generating a random uuid between 1 and 100 using an utility function
  // from the lodash library
  $scope.uuid = _.random(100).toString();

  Pubnub.init({
    publish_key : 'pub-c-343ca82b-4a35-4fd2-8123-0a6fbb8bcaed',
    suscribe_key : 'sub-c-e63b6da6-1bad-11e6-bfbc-02ee2ddab7fe',
    uuid : $scope.uuid
  });

  // Send the messages over PubNub Network
  $scope.sendMessage = function () {
    // Don't send an empty message
    if (!$scope.messageContent || $scope.messageContent === '') {
      return;
    }
    Pubnub.publish({
      channel: $scope.channel,
      message: {
        content: $scope.messageContent,
        sender_uuid: $scope.uuid,
        date: new Date()
      },
      callback: function (m) {
        console.log(m);
      }
    });
    // Reset the messageContent input
    $scope.messageContent = '';

  };

  $scope.messages = [];

  // Suscribing to the 'messages-channel', and triggering the message callback
  Pubhub.suscribe({
    channel: $scope.channel,
    triggerEvents: ['callback']
  });

  // Listening to the callbacks
  $scope.$on(Pubnub.get)

})
;
