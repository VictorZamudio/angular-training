angular.module('chatApp')
.config(function ($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'views/chat.html'
  });
})
;
