angular.module("chatApp")
.directive("messageItem", function (messageService){
  return {
    restrict: "E",
    templateUrl: 'message-item.html',
    scope: {
      senderUiid: "@",
      content: "@",
      date: "@"
    }
  }
})
;
