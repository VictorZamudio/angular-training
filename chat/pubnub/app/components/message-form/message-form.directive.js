angular.module('chatApp')
.directive('messageForm', function () {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'messageForm.html',
    scope: {},
    controller: function ($scope, currentUser, messageService) {
      $scope.uuid = currentUser;
      $scope.messageContent = '';
      $scope.sendMessage = function () {
        messageService.sendMessage($scope.messageContent);
        $scope.messageContent = '';
      };
    }
  }
})
;
