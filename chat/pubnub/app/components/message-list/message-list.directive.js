angular.module('chatApp')
.directive('messageList', function ($timeout, $anchorScroll, MessageService, ngNotify) {
  return {
    restric: "E",
    replace: true,
    templateUrl: 'message-list.html',
    link: function (scope, element, attrs, ctrl) {
      var element = angular.element(element);

      var scrollToBottom = function () {
        element.scrollTop(element.prop('scrollHeight'));
      };

      var hasScrollReachedBottom = function () {
        return element.scrollTop() + element.innerHeight() >= element.prop('scrollHeight');
      };

      var hasScrollReachedTop = function () {
        return element.scrollTop() === 0;
      };

      var fetchPreviousMessages = function () {
        ngNotify.set('loading previous messages...', 'success');
        var currentMessage = MessageService.getMessages()[0].uuid;
        MessageService.fetchPreviousMessages().then(function (m) {
          // Scroll to the previous message
          $anchorScroll(currentMessage);
        });
      };

      var watchScroll = function () {
        if (hasScrollReachedTop()) {
          if (MessageService.messagesAllFetched()) {
            ngNotify.set('All the messages have been loaded', 'grimace');
          } else {
            fetchPreviousMessages();
          }
        }
        scope.autoScrollDown = hasScrollReachedBottom();
      };

      var init = function(){
        // Scroll down when the list is populated
        var unregister = $rootScope.$on('factory:message:populated', function () {
          _.defer(scrollToBottom);
          // Unbind the event as we won't need it in the future
          unregister();
        });

        // Scroll down when the list is rendered
        $timeout(scrollToBottom, 400);

        // Scroll down when new message
        MessageService.subscribeNewMessage(function(){
          if (scope.autoScrollDown) {
            scrollToBottom();
          }
        });

        element.bind('scroll', _.debounce(watchScroll, 250));
      };
      init();
    },
    controller: function ($scope) {
      $scope.messages = MessageService.getMessages();
    }
  }
})
;
