angular.module('chatApp')
.factory('MessageService', ['$rootScope', '$q', 'Pubnub', 'currentUser',
  function messageServiceFactory($rootScope, $q, Pubnub, currentUser) {
    // Aliasing this by self so we can access to this in the inner functions
    var self = this;
    this.messages = [];
    this.channel = 'messages-chanel';
    // We keep track of the timetoken of the first message of the array
    // so it will be easier to fetch the previous messages later
    this.firstMessageTimeToken = null;
    this.messagesAllFetched = false;
    // The service will return useful functions to interact with
    return {};

    var init = function(){
      Pubnub.subscribe({
        channel: self.channel,
        triggerEvents: ['callback']
      });

      // Store the new messages received in the messages array
      subscribeNewMessage(function (ngEvent, m) {
        self.messages.push(m);
        // used for the entire app can be aware that the message factory
        // has changed
        $rootScope.$digest();
      });
    };

    var populate = function () {
      var defaultMessagesNumber = 20;
      Pubnub.history({
        channel: self.channel,
        callback: function(m){
          // Update the timetoken of the first message
          angular.extend(self.messages, m[0]);

          if(m[0].length < defaultMessagesNumber){
            self.messagesAllFetched = true;
          }

          self.timeTokenFirstMessage = m[1];
          $rootScope.$digest();
          $rootScope.$emit('factory:message:populated');
        },
        count: 20
      });
    };


    /////////////////////// PUBLIC API /////////////////////////

    var subscribeNewMessage = function (callback) {
      $rootScope.$on(Pubnub.getMessageEventNameFor(self.channel), callback);
    };

    var getMessages = function () {
      if (_.isEmpty(self.messages)) {
        populate();
      }
      return self.messages;
    };

    var sendMessage = function (messageContent) {
      // Don send a empty message
      if(_.isEmpty(messageContent)){
        return;
      }

      Pubnub.publish({
        channel: self.channel,
        message: {
          uuid: (Date.now() + currentUser),
          content: messageContent,
          sender_uuid: currentUser,
          date: Date.now()
        }
      });
    };

    var fetchPreviousMessages = function () {
      var defaultMesagesNumber = 20;
      var deferred = $q.defer();

      Pubnub.history({
        channel: self.channel,
        callback: function(m){
          // update the timeToken of the first message
          self.timeTokenFirstMessage = m[1];
          Array.prototype.unshift.apply(self.mesages, m[0]);

          if (m[0].length < defaultMesagesNumber ) {
            self.messagesAllFetched = true;
          }

          $rootScope.$digest();
          deferred.resolve(m);
        },
        error: function(m){
          deferred.reject(m);
        },
        count: 10,
        start: self.timeTokenFirstMessage,
        reverse: false

      });

      return deferred.promise;
    };


    init();
}])
;
